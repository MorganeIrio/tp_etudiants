module.exports = function(grunt) {
    grunt.initConfig({
        //On lit le fichier json
        pkg: grunt.file.readJSON('package.json'),

        sprite:{
            all: {
                src: 'img/*.png',
                dest: 'spritesheet/spritesheet.png',
                destCss: 'css/sprites.css'
              }
            }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.loadNpmTasks('grunt-spritesmith'); 

        


    grunt.registerTask('default', ['concat', 'uglify','qunit']);


};