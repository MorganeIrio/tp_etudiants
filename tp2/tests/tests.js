QUnit.test( "a basic test example", function( assert ) {
    var value = "hello";
    assert.equal( value, "hello", "We expect value to be hello" );
});
QUnit.test( "nombre pair", function( assert ) {

    assert.expect(6);

    var result1 = NbPair(2);
    var result2 = NbPair(4);
    var result3 = NbPair(6);
    var result4 = NbPair(8);
    var result5 = NbPair(10);
    var result6 = NbPair(12);

    assert.equal(result1,true, "Première assertion true");
    assert.equal(result2,true, "Deuxième assertion true");
    assert.equal(result3,true, "Troisième assertion true");
    assert.equal(result4,true, "Quatrième assertion true");
    assert.equal(result5,true, "Cinquième assertion true");
    assert.equal(result6,true, "Sixième assertion true");

});
QUnit.test("concatenation", function( assert ){

    assert.expect(5);

    var result1 = ChaineConcat("Tom");
    var result2 = ChaineConcat("Pierre");
    var result3 = ChaineConcat("Marie");
    var result4 = ChaineConcat("Emilie");
    var result5 = ChaineConcat("Benjamin");

    assert.equal(result1,"Bonjour Tom", "Première concaténation");
    assert.equal(result2,"Bonjour Pierre", "Deuxième concaténation");
    assert.equal(result3,"Bonjour Marie", "Troisième concaténation");
    assert.equal(result4,"Bonjour Emilie", "Quatrième concaténation");
    assert.equal(result5,"Bonjour Benjamin", "Quatrième concaténation");

});
QUnit.test("validation NIR", function( assert ){

    var result1 = nirValidator("19235636784653");
    var result2 = nirValidator("177097645103210");
    var result3 = nirValidator("bonjour");

    assert.equal(result1,false);
    assert.equal(result2,true);
    assert.equal(result3,false);
});
QUnit.test("validation RIB", function( assert ){
    assert.expect(3);

    assert.ok(isRIBvalid("11111","22222","ABCD3333EFG","42"));
    assert.ok(isRIBvalid("30002","00550", "0000157845Z", "02"));
    assert.notOk(isRIBvalid("30002","00110", "0000153845Z", "02"));
});
/*
QUnit.test("retour clé Nir", function( assert ){
    assert.ok(nirValidator("177097645103210"));
    assert.equal(02,cleNir("177097645103210"));
});*/
