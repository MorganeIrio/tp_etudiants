/*
var nirRegex = /[12][ \.\-]?[0-9]{2}[ \.\-]?(0[1-9]|[1][0-2])[ \.\-]?([0-9]{2}|2A|2B)[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}/

    // assuranceCode must be a string
    function nirValidator(assuranceCode){          
        var num_short = assuranceCode.slice(-2);
        return ( 97 - (num_short % 97 ));
    }
*/;function NbPair(Nb)
{
   if(Nb%2 == 0) return true;
   else return false;
}

function ChaineConcat(Chaine)
{
   return "Bonjour "+Chaine;
}
;
var nirRegex = /[12][ \.\-]?[0-9]{2}[ \.\-]?(0[1-9]|[1][0-2])[ \.\-]?([0-9]{2}|2A|2B)[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}/

    // assuranceCode must be a string
    function nirValidator(assuranceCode){          
        if(nirRegex.test(assuranceCode)){
            var nir = Number(
                assuranceCode
                .replace("2A","19")
                .replace("2B","18")
                .slice(0, assuranceCode.length - 2)
            );
            return ( 97 - nir % 97 ) == Number( assuranceCode.slice(-2) );
        } else {
            return false;
        }
    };function isRIBvalid(bqe,gui,cpttmp,rib){
    var cpt=cpttmp.toUpperCase();
    var tab= "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var tab1="123456789123456789234567890123456789".split("");

    while (cpt.match(/\D/) != null)
    cpt=cpt.replace(/\D/, tab1[tab.indexOf(cpt.match(/\D/))]);
    var cp=parseInt    (cpt, 10);

    a=bqe%97;
    a=a*100000+parseInt(gui, 10);
    a=a%97;
    a=a*Math.pow(10, 11) + cp;
    a=a%97
    a=a*100;
    a=a%97
    a=97-a;
    
    if (a==rib)
        return true;
    else
    {
    return false;
    }

}
;QUnit.test( "a basic test example", function( assert ) {
    var value = "hello";
    assert.equal( value, "hello", "We expect value to be hello" );
});
QUnit.test( "nombre pair", function( assert ) {

    assert.expect(6);

    var result1 = NbPair(2);
    var result2 = NbPair(4);
    var result3 = NbPair(6);
    var result4 = NbPair(8);
    var result5 = NbPair(10);
    var result6 = NbPair(12);

    assert.equal(result1,true, "Première assertion true");
    assert.equal(result2,true, "Deuxième assertion true");
    assert.equal(result3,true, "Troisième assertion true");
    assert.equal(result4,true, "Quatrième assertion true");
    assert.equal(result5,true, "Cinquième assertion true");
    assert.equal(result6,true, "Sixième assertion true");

});
QUnit.test("concatenation", function( assert ){

    assert.expect(5);

    var result1 = ChaineConcat("Tom");
    var result2 = ChaineConcat("Pierre");
    var result3 = ChaineConcat("Marie");
    var result4 = ChaineConcat("Emilie");
    var result5 = ChaineConcat("Benjamin");

    assert.equal(result1,"Bonjour Tom", "Première concaténation");
    assert.equal(result2,"Bonjour Pierre", "Deuxième concaténation");
    assert.equal(result3,"Bonjour Marie", "Troisième concaténation");
    assert.equal(result4,"Bonjour Emilie", "Quatrième concaténation");
    assert.equal(result5,"Bonjour Benjamin", "Quatrième concaténation");

});
QUnit.test("validation NIR", function( assert ){

    var result1 = nirValidator("19235636784653");
    var result2 = nirValidator("177097645103210");
    var result3 = nirValidator("bonjour");

    assert.equal(result1,false);
    assert.equal(result2,true);
    assert.equal(result3,false);
});
QUnit.test("validation RIB", function( assert ){
    assert.expect(3);

    assert.ok(isRIBvalid("11111","22222","ABCD3333EFG","42"));
    assert.ok(isRIBvalid("30002","00550", "0000157845Z", "02"));
    assert.notOk(isRIBvalid("30002","00110", "0000153845Z", "02"));
});
/*
QUnit.test("retour clé Nir", function( assert ){
    assert.ok(nirValidator("177097645103210"));
    assert.equal(02,cleNir("177097645103210"));
});*/
