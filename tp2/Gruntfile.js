module.exports = function(grunt) {
    grunt.initConfig({
        //On lit le fichier json
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            options: {
              // define a string to put between each file in the concatenated output
              separator: ';'
            },
            dist: {
              // the files to concatenate
              src: ['js/*.js','tests/*.js'],
              // the location of the resulting JS file
              dest: 'dist/<%= pkg.name %>.js'
            }
          },

          uglify: {
            options: {
              // the banner is inserted at the top of the output
              banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
              files: {
                'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
              }
            }
          },

          watch: {
            options:{
              livereload:true
            },
            files: ['js/*.js','tests/tests.js','html/*'],
            tasks: ['concat', 'uglify','qunit']
          },

          qunit: {
            files: ['html/test.html']
          },
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-qunit');    
        


    grunt.registerTask('default', ['concat', 'uglify','qunit']);


};