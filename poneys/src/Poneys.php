<?php 
  class Poneys {
      private $count;

      //private $names = array("Fleur", "Lion","Cactus","Abricot");

      public function getCount() {
        return $this->count;
      }

      public function setCount($number) {
        $this->count = $number;
      }

      public function removePoneyFromField($number){
        if ($number < 0){
          throw new InvalidArgumentException;
        }else{
          $this->count -= $number;
        }
      }

      public function getNames() {
      
      }

      public function addPoneyToField($number){
        $this->count += $number;
      }

      public function fieldNotFull(){
        //Si il reste au moins 1 place, on retourne vrai
        if ($this->count < TAILLE_CHAMP){
          return TRUE;
        } else {
          //sinon on retourne faux
          return FALSE;
        }
      }
  }
?>
