<?php
  require_once 'src/Poneys.php';
  use \PHPUnit\Framework\TestCase;

  class PoneysTest extends Testcase {

    private $poneys;

    public function setUp(){
      $this->poneys = new Poneys();
      $this->poneys->setCount(QUANTITE_PONEY);
    }

    public function tearDown(){
      $this->poneys=null;
    }

    public function test_removePoneyFromField() {
      // Setup
      //génère le champ avec les poneys dedans
    
      // Action
      $this->poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->poneys->getCount());
    }

    public function test_addPoneyToField(){
      // Setup
      

      // Action
      $this->poneys->addPoneyToField(2);

      // Assert
      $this->assertEquals(10,$this->poneys->getCount());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function test_removePoneyFromFieldNegative(){
      //setup
    

      $this->poneys->removePoneyFromField(-3);
    }

    //On ajoute la méthode DataProvider:
    public function addDataProvider(){
      return array(
        array(2,6),
        array(3,5),
      );
    }

    /**
     * @dataProvider addDataProvider
     */
    public function test_RemovePoneyProvider($a, $expected){
      //Setup
     

      $this->poneys->removePoneyFromField($a);

      $this->assertEquals($expected,$this->poneys->getCount());
    }

    public function test_AvailablePlace(){
     

      $this->assertTrue($this->poneys->fieldNotFull());
    }

    public function test_NoAvailablePlace(){
      

      $this->poneys->addPoneyToField(7);
      //Il y a en tout 15 poneys dans le champ

      $this->assertFalse($this->poneys->fieldNotFull());
    }

    public function test_getNames(){
      //création du Mock, récupération du mock Poneys
      $this->poneys = $this->getMockBuilder('Poneys')->getMock();

      //La on dit que la methode getNames doit renvoyer exactement 
      //1 fois les valeurs citées
      $this->poneys
        ->expects($this->exactly(1))
        ->method('getNames')
        ->willReturn(
          array("Fleur", "Lion","Cactus","Abricot")
        );

        //test du mock
        $this->assertEquals(
          array("Fleur", "Lion","Cactus","Abricot"),
          $this->poneys->getNames()
        );
    }

    public function test_setCount(){
      $this->poneys->setCount(9);
      $this->assertEquals(9,$this->poneys->getCount());
    }


  }
 ?>
