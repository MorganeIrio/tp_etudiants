<?php
  require_once 'src/Poneys.php';
  use \PHPUnit\Framework\TestCase;

  class NewTestClass extends Testcase {
    private $poneys;
    
        public function setUp(){
          $this->poneys = new Poneys();
          $this->poneys->setCount(QUANTITE_PONEY);
        }
    
        public function tearDown(){
          $this->poneys=null;
        }
    
        public function test_removePoneyFromField() {
          // Setup
          //g�n�re le champ avec les poneys dedans
        
          // Action
          $this->poneys->removePoneyFromField(3);
          
          // Assert
          $this->assertEquals(5, $this->poneys->getCount());
        }
    
  }

  ?>